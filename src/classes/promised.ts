import { TConsumer } from '../general/types';

export interface IPromised<T> {
   readonly promise: Promise<T>;
}

export class Promised<T> implements IPromised<T> {
   protected _resolver!: TConsumer<T>;
   protected _rejecter!: TConsumer<string | object>;
   readonly promise = new Promise<T>((resolve, reject) => {
      this._resolver = resolve;
      this._rejecter = reject;
   });


   constructor(
      readonly ttlMs?: number
   ) {
      this.setTimer();
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   public resolve(value: T) {
      this._resolver(value);
   }


   public reject(reason: any) {
      this._rejecter(reason);
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected setTimer() {
      if (!this.ttlMs) return; // #Return#
      setTimeout(() => {
         this.reject(new Error(`Expired. Waiting more than ${this.ttlMs} Ms`));
      }, this.ttlMs);
   }
}
