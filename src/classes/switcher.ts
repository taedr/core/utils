import { TFunction } from '../general/types';

// ==================================================
//                   Core
// ==================================================
export type TSwitchState = 'ON' | 'OFF';

export abstract class ASwitch {
   protected _isAttached?: boolean;
   protected _state: TSwitchState = 'OFF'

   public get isOn(): boolean { return this._state === 'ON'; }
   public get isOff(): boolean { return this._state === 'OFF'; }
   public get state(): TSwitchState { return this._state; }
   public set state(state: TSwitchState) { this.setState(state); }

   /**
    * Switch that performs some actions when changing between `ON` / `OFF` state.
    * Can be attached to the Hub.
    * State changing is done by setting `state` field.
    * Default state is `OFF`.
    * */
   constructor(
      state: TSwitchState = 'OFF'
   ) {
      this.state = state;
   }
   // ---------------------------------------------
   //                   Abstract
   // ---------------------------------------------
   protected abstract on(): void;
   protected abstract off(): void;
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected setState(state: TSwitchState) {
      if (this._isAttached) throw new Error(SWITCH_ERRORS.manual); // #Error#
      if (this.state === state) return; // #Return#
      this._state = state;
      if (state === 'ON') this.on();
      if (state === 'OFF') this.off();
   }
}
// ==================================================
//                   Switch
// ==================================================
export interface ISwitch {
   readonly on: TFunction;
   readonly off: TFunction;
}

export class Switch extends ASwitch {
   constructor(
      protected _ctx: ISwitch,
      state: TSwitchState = 'OFF'
   ) {
      super(state);
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected on() { this._ctx.on(); }
   protected off() { this._ctx.off(); }
}
// ==================================================
//                   Hub
// ==================================================
export class Hub extends ASwitch {
   protected _switchers = new Set<ASwitch>();

   /**
    * Hub that accumulates switches and changes their state simultaneously, as
    * soon as it's own state changes.
    * Default state is `ON`.
    * */
   constructor(state: TSwitchState = 'ON') {
      super(`OFF`);
      this.state = state;
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   /**
    * Attaches `switcher` to the hub.
    * From now on `switcher` state can't be changed from another place, than by Hub.
    * `switcher` state will reflect the Hub state.
    * */
   public attach(switcher: ISwitch): Switch;
   public attach<T extends ASwitch>(switcher: T): T;
   public attach<T extends ASwitch>(sw: ISwitch | T): Switch | T {
      const _sw = sw instanceof ASwitch ? sw : new Switch(sw);

      if (_sw instanceof Hub) {
         if (_sw === this as Hub) throw new Error(SWITCH_ERRORS.itself); // #Error#
         (_sw as Hub).checkAttach(this);
      } else {
         if (_sw['_isAttached']) throw new Error(SWITCH_ERRORS.few); // #Error#
      }

      this.toggle(_sw, this.state);
      this._switchers.add(_sw);

      return _sw;
   }
   /**
    * Detaches `switcher` from the hub.
    * From now on `switcher` state can be changed manually.
    * */
   public detach(switcher: ASwitch) {
      this.toggle(switcher, 'OFF');
      delete switcher['_isAttached'];
      return this._switchers.delete(switcher);
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected on() { this.toggleAll(`ON`); }
   protected off() { this.toggleAll(`OFF`); }


   protected toggle(switcher: ASwitch, state: TSwitchState) {
      switcher['_isAttached'] = false;
      switcher.state = state;
      switcher['_isAttached'] = true;
   }


   protected toggleAll(state: TSwitchState) {
      this._switchers.forEach(sw => this.toggle(sw, state));
   }


   protected checkAttach(hub: Hub) {
      for (const sw of this._switchers) {
         if (sw instanceof Hub) {
            if (sw === hub) throw new Error(SWITCH_ERRORS.recursive); // #Error#
            sw.checkAttach(hub);
         }
      }
   }
}
// ==================================================
//                   Errors
// ==================================================
export const SWITCH_ERRORS = {
   itself: `Can't attach to itself.`,
   few: `Switcher is already attached. Can't attach to few places.`,
   manual: `Attached Switcher state can't be changed manually.`,
   recursive: `Recursive hub attach`
}
