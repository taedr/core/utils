import { IS } from "../other/typeof";

export class Initer<T extends object> {
   private _initer?: T;
   protected message = `${this.classId} - you must provide default config with` +
      `'${this.classId}.init(initer)' ` +
      `before creating any of ${this.classId} instances.`;

   public get value(): T {
      if (IS.null(this._initer)) { // #Error#
         throw new Error(this.message);
      }
      return this._initer;
   }

   constructor(
      protected classId: string,
   ) { }
   // ---------------------------------------------
   //                Api
   // ---------------------------------------------
   public init(initer: T) {
      if (this._initer !== null) { // #Error#
         throw new Error(`${this.classId} - Attempt to init few times`);
      }

      this._initer = initer;
   }
}
