import { SeacrhError } from '../general/errors';
import { IS } from '../other/typeof';


export function notFoundDec(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
   let originalMethod = descriptor.value;

   descriptor.value = function (...args: any[]) {
      let result = originalMethod.apply(this, args);
      if (IS.null(result)) {
         throw new SeacrhError();
      } else {
         return result;
      }
   }
}