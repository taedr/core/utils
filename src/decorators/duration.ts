
export function durationMsDec(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
   const originalMethod = descriptor.value;
   const name = originalMethod.name ?? ``;

   descriptor.value = function (...args: any[]) {
      const start = performance.now();
      const result = originalMethod.apply(this, args);
      const duration = performance.now() - start;

      console.log(`Duration '${name}': ${duration} Ms`);

      return result;
   }
}
