import { TStringKeys } from "../index";

export interface IKeysMap<T extends object, V> extends Map<TStringKeys<T>, V> {
   get<K extends TStringKeys<T>>(key: K): V;
}
