import { TProducer, ToNumber, ToString } from '../general/types';
import { getHrTime } from '../other/performance';
import { IId } from '../other/id';

export const FN_MIRROR = <T>(val: T) => val;
export const FN_RUN = <T>(fn: TProducer<T>) => fn();
export const FN_TO_STRING = <T>(val: T) => val + ``;
export const FN_COPY = (val: object): object => JSON.parse(JSON.stringify(val));
export const FN_PACK = (count: number, start = 0) => new Array<number>(count).fill(0).map((_, i) => i + start);
export const FN_DELAY = <T>(timeMs: number) => new Promise<T>(resolve => setTimeout(resolve, timeMs));
export const FN_GET_ID = ({ id }: IId) => id;
export const FN_GET_ID_STR = ({ id }: IId) => id + ``;
export const FN_GET_ID_NUM = ({ id }: IId) => id + ``;
export const FN_GET_VALUE = <T>({ value }: { value: T }) => value;
export const FN_CONCAT = <T>(iterables: Iterable<Iterable<T>>): T[] => {
   const concated: T[] = [];
   for (const iterable of iterables) concated.push(...iterable);
   return concated;
}
// ===============================================
//                   Producers
// ===============================================
export const FN_FALSE = () => false;
export const FN_TRUE = () => true;
export const FN_STRING = () => '';
export const FN_ZERO = () => 0;
export const FN_NULL = () => null;
export const FN_RESOLVE = <T>(val?: T, timeMs = 0) => new Promise<T | undefined>(resolve => {
   setTimeout(() => resolve(val), timeMs)
});
export const FN_REJECT = <T>(val?: T, timeMs = 0) => new Promise<T>((_, reject) => {
   setTimeout(() => reject(val), timeMs)
});
// ===============================================
//                   Sorters
// ===============================================
export const FN_DATE_SORTER = (a: Date, b: Date) => +a - +b;
export const FN_NUMBER_SORTER = <T>(get: ToNumber<T>) => (a: T, b: T) => get(a) - get(b);
export const FN_STRING_SORTER = <T>(get: ToString<T>) => (a: T, b: T) => get(a).localeCompare(get(b));
export const FN_BOOLEAN_SORTER = (a: boolean, b: boolean) => +a - +b;
// ===============================================
//                   Random
// ===============================================
export const FN_RANDOM_BOOL = (): boolean => !!Math.round(Math.random());
export const FN_RUNTIME_ID = () => getHrTime().toString(36).toUpperCase();
