export interface IError {
   readonly code: number;
   readonly message: string;
}


export class SeacrhError extends Error implements IError {
   readonly code = 404;
   
   constructor() {
      super(`Searched value not found`);
   }
}