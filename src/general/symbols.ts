export const PREV = Symbol(`PREV`);
export const NEXT = Symbol(`NEXT`);
export const REPEAT = Symbol(`REPEAT`);

export const CHECK = Symbol(`CHECK`);
export const SKIP = Symbol(`SKIP`);


export const ARRAY = Symbol(`ARRAY`);
export const OBJECT = Symbol(`OBJECT`);

export const VALUE = Symbol(`VALUE`);
export const WRAP = Symbol(`WRAP`);
export const COMMON = Symbol(`COMMON`);
export const EMPTY = Symbol(`EMPTY`);

export type SArray = typeof ARRAY;
export type SObject = typeof OBJECT;
export type SValue = typeof VALUE;
export type SWrap = typeof WRAP;
export type SCommon = typeof COMMON;
export type SEmpty = typeof EMPTY;