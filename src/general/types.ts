// ===============================================
//                Interfaces
// ===============================================
export interface IMap<T> {
   [id: string]: T;
}


export interface IMinMax {
   readonly min: number;
   readonly max: number;
}


export type TDataType = 'array' | 'object';
export type TExecMode = 'SYNC' | 'ASYNC';
// ===============================================
//                   Constants
// ===============================================
export const DIVIDER = Object.freeze({
   TWO_DOT: ' : ',
   LINE: ' | ',
   ARROW: ' -> ',
});



/** Mark showing that value should be mutated directly. */
export const MUTATION = Symbol(`MUTATION`);
/** Mark showing that value should NOT be mutated directly. Before any actions should create a copy. */
export const IMMUTABLE = Symbol(`IMMUTABLE`);

export type TModification = typeof IMMUTABLE | typeof MUTATION;

export const OVERLOAD_ERROR = `Function overload - invalid arguments combination`;
// ===============================================
//                Execution
// ===============================================
/** Marks that execution will be performed before dom render. */
export const DOM_RENDER = Symbol(`DOM_RENDER`);
export const SYNC = Symbol(`SYNC`);
export const ASYNC = Symbol(`ASYNC`);
// ===============================================
//                Types
// ===============================================
export type TPrimitive = string | number | boolean | bigint;
export type TReadonlyArray<T> = ReadonlyArray<Readonly<T>>;
export type TFunction = () => void;
// -----------------------------------------------
//                   Functions
// -----------------------------------------------
export type TIsEqual<T> = (a: T, b: T) => boolean;
export type TConsumer<T> = (value: T) => void;
export type TProducer<T> = () => T;
export type TSorter<T> = (a: T, b: T) => number;
export type TReducer<A, T> = (acc: A, value: T) => A;
export type TNew<T> = new (...args: any[]) => T;
// -----------------------------------------------
//                   Mappers
// -----------------------------------------------
export type TMapper<I, O> = (input: I) => O;
export type ToPrimitive<T> = TMapper<T, TPrimitive>;
export type ToBoolean<T> = TMapper<T, boolean>;
export type ToString<T> = TMapper<T, string>;
export type ToNumber<T> = TMapper<T, number>;
export type ToPromise<I, O> = TMapper<I, Promise<O>>;
export type ToUnknown<T> = TMapper<T, unknown>;
// -----------------------------------------------
//                   DeepPartial
// -----------------------------------------------
export type TDeepPartial<T, N extends null | undefined = never> = T extends Function
   ? T
   : (T extends object
      ? T extends unknown[]
      ? TDeepPartial<T[number] | N>[]
      : { [P in keyof T]?: TDeepPartial<T[P] | N>; }
      : T | N
   );
