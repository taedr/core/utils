import { IS } from './typeof';

export function convertToForm(object: object): FormData {
   const form = new FormData();
   const prefix = ``;
   convert(object, form, prefix);
   return form;
}


function convert(object: object, form: FormData, prefix: string) {
   for (let [key, value] of Object.entries(object)) {
      key = prefix ? `${prefix}[${key}]` : key;

      if (value instanceof Date) {
         form.append(key, value.toISOString());
      } else if (value instanceof File) {
         form.append(key, value);
      } else if (IS.array(value)) {
         for (let i = 0; i < value.length; i++) {
            const entry: any = value[i];
            const _prefix = `${key}[${i}]`;

            if (IS.object(entry)) {
               convert(entry, form, _prefix);
            } else {
               form.append(_prefix, entry.toString());
            }
         }
      } else if (IS.object(value)) {
         convert(value, form, key);
      } else {
         form.append(key, value.toString());
      }
   }
}
