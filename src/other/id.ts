export type TGetId<T> = (value: T) => string | number;


export interface IId {
   readonly id: number | string;
}

export class IdNum {
   public id = 0;
}


export class IdStr {
   public id = ``;
}

let time = Date.now();
let count = 0;

export function getRuntimeId(): string {
   const now = Date.now();
   if (time < now) {
      time = now;
      count = 0;
   }

   return `${time}_${count++}`;
}
