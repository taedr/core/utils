import { IS } from "./typeof";

export type TIterableGet<T> = (index: number, prev: T | null) => T;
export type TIterableEnd<T> = (index: number, prev: T | null) => void;


export interface IIterator<T> {
   readonly next: TIterableGet<T>;
   readonly onEnd?: TIterableEnd<T>;
}


export function iterator<T>({ next, onEnd }: IIterator<T>) {
   return function () {
      let index = 0;
      let prev: T | null = null;

      return {
         next() {
            const value = next(index, prev);
            const done = IS.null(value);

            // const result: IteratorResult<T> = {done: true, value:}

            if (done && onEnd) onEnd(index, prev);

            index += 1;
            prev = value;

            return { done, value };
         },
         return() {
            const value: T | null = null;
            const done: boolean = true;
            // const result: IteratorResult<T> = {done: true, value:}

            if (onEnd) onEnd(index, prev);

            return { done, value };
         }
      };
   }
};
