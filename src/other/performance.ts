import { TProducer } from '../general/types';
import { IS } from './typeof';

export interface IPerformance<T> {
   readonly duration: number;
   readonly result: T
}


export function performanceMs<T>(func: TProducer<T>): IPerformance<T> {
   if (IS.running.node) return performanceNode(func);
   return performanceBrowsers(func);
}


export function performanceBrowsers<T>(func: TProducer<T>): IPerformance<T> {
   const start = performance.now();
   const result = func();
   const duration = performance.now() - start;

   return { result, duration }
}


export function performanceNode<T>(func: TProducer<T>, metrics: 'mili' | 'micro' | 'nano' = 'mili'): IPerformance<T> {
   const start = process.hrtime.bigint();
   const result = func();
   const _duration = process.hrtime.bigint() - start;
   let duration: bigint;

   switch (metrics) {
      case 'mili': duration = _duration / BigInt(1000_000); break;
      case 'nano': duration = _duration; break;
      case 'micro': duration = _duration / BigInt(1000); break;
   }

   return { duration: Number(duration), result };
}



export function getHrTime(): number {
   if (IS.running.node) return Number(process.hrtime.bigint() / BigInt(1000_000));
   return performance.now();
}
