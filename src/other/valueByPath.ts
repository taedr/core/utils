import { TZPack, getZPack,  } from './pack';
import { IS } from './typeof';

export function getValueByPath(object: object, path: TZPack<string>): unknown {
   const arr = getZPack(path);
   if (arr.length === 0) return object;
   const fields = arr.length === 1 ? arr[0].split(`.`) : arr;
   let value: any = object;

   while (fields.length !== 0 && !IS.null(value)) {
      value = value[fields.shift() as string] ?? null;
   }

   return value;
}
