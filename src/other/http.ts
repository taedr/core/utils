export type THttpProtocol = 'http' | 'https';
export type TWsProtocol = 'ws' | 'wss';
export type TRequestProtocol = THttpProtocol | TWsProtocol;
export type TRequestMethod = 'GET' | 'POST' | 'PUT' | 'DELETE';
