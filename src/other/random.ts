import { IS } from './typeof';

export interface IRandomNumsPack {
   readonly min: number;
   readonly max: number;
   readonly size: number;
   readonly rounded?: boolean;
   readonly unique?: boolean;
}

export function FN_RANDOM_NUM(min: number, max: number, rounded?: boolean): number
export function FN_RANDOM_NUM(ctx: IRandomNumsPack): number[]
export function FN_RANDOM_NUM(): number | number[] {
   const first = arguments[0];
   if (IS.object(first)) {
      const { size, min, max, rounded, unique } = first as IRandomNumsPack;
      const nums: number[] = [];
      const pusher = unique ? _unique : _nonUnique;

      while (nums.length !== size) {
         const num = getRandomNum(min, max, rounded);
         pusher(nums, num);
      }

      return nums;
   } else {
      return getRandomNum(...(Array.from(arguments) as [number, number, boolean]));
   }
}

function _unique(nums: number[], num: number) {
   if (!nums.includes(num)) nums.push(num);
}

function _nonUnique(nums: number[], num: number) {
   nums.push(num);
}

function getRandomNum(min: number, max: number, rounded = true) {
   let num = Math.random() * (max - min) + min;
   if (rounded) num = Math.round(num);
   return num;
}