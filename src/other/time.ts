export const SECOND_MS = 1000;
export const MINUTE_MS = 60 * SECOND_MS;
export const HOUR_MS = 60 * MINUTE_MS;
export const DAY_MS = 24 * HOUR_MS;
export const WEEK_MS = DAY_MS * 7;
export const MONTH_MS = DAY_MS * 30.5;
export const YEAR_MS = DAY_MS * 365;

