import { TNew } from '../general/types';

export function getStaticChain(rootBuilder: TNew<object>, field: string | number | symbol): unknown[] {
   const values: any[] = [];
   let builder: any = rootBuilder;

   while(builder !== Object) {
      const hasField = builder.hasOwnProperty(field);
      if (hasField) values.push(builder[field]);
      builder = builder['prototype']['__proto__']['constructor'];
   }

   return values.reverse();
}
