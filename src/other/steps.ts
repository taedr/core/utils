import { TFMorph, getMorph } from './morph';
import { IS } from './typeof';


/** 
 * `fns` - functions that will be called one after another
 * with return result from previous step as an argument.
 * End result is the result of last `fn` call.
 */
export function steps<A, B, C>(fn_1: TFMorph<A, B>, fn_2: TFMorph<B, C>): TFMorph<A, C>;
export function steps<A, B, C, D>(
   fn_1: TFMorph<A, B>, fn_2: TFMorph<B, C>,
   fn_3: TFMorph<C, D>
): TFMorph<A, D>;
export function steps<A, B, C, D, E>(
   fn_1: TFMorph<A, B>, fn_2: TFMorph<B, C>,
   fn_3: TFMorph<C, D>, fn_4: TFMorph<D, E>
): TFMorph<A, E>;
export function steps<A, B, C, D, E, F>(
   fn_1: TFMorph<A, B>, fn_2: TFMorph<B, C>, fn_3: TFMorph<C, D>,
   fn_4: TFMorph<D, E>, fn_5: TFMorph<E, F>
): TFMorph<A, F>;
export function steps<A, B, C, D, E, F, G>(
   fn_1: TFMorph<A, B>, fn_2: TFMorph<B, C>, fn_3: TFMorph<C, D>,
   fn_4: TFMorph<D, E>, fn_5: TFMorph<E, F>, fn_6: TFMorph<F, G>
): TFMorph<A, G>;
export function steps<A, B, C, D, E, F, G, H>(
   fn_1: TFMorph<A, B>, fn_2: TFMorph<B, C>, fn_3: TFMorph<C, D>, fn_4: TFMorph<D, E>,
   fn_5: TFMorph<E, F>, fn_6: TFMorph<F, G>, fn_7: TFMorph<G, H>
): TFMorph<A, H>;
export function steps<A, B, C, D, E, F, G, H, I>(
   fn_1: TFMorph<A, B>, fn_2: TFMorph<B, C>, fn_3: TFMorph<C, D>, fn_4: TFMorph<D, E>,
   fn_5: TFMorph<E, F>, fn_6: TFMorph<F, G>, fn_7: TFMorph<G, H>, fn_8: TFMorph<H, I>
): TFMorph<A, I>;
export function steps<A, B, C, D, E, F, G, H, I, J>(
   fn_1: TFMorph<A, B>, fn_2: TFMorph<B, C>, fn_3: TFMorph<C, D>, fn_4: TFMorph<D, E>, fn_5: TFMorph<E, F>,
   fn_6: TFMorph<F, G>, fn_7: TFMorph<G, H>, fn_8: TFMorph<H, I>, fn_9: TFMorph<I, J>
): TFMorph<A, J>;
export function steps<A, B, C, D, E, F, G, H, I, J, K>(
   fn_1: TFMorph<A, B>, fn_2: TFMorph<B, C>, fn_3: TFMorph<C, D>, fn_4: TFMorph<D, E>, fn_5: TFMorph<E, F>,
   fn_6: TFMorph<F, G>, fn_7: TFMorph<G, H>, fn_8: TFMorph<H, I>, fn_9: TFMorph<I, J>, fn_10: TFMorph<J, K>
): TFMorph<A, K>;
export function steps<T>(fns: TFMorph<T, T>[]): TFMorph<T, T>;
export function steps(
   first: TFMorph<unknown, unknown>[] | TFMorph<unknown, unknown>,
   ...fns: TFMorph<unknown, unknown>[]
): TFMorph<unknown, unknown> {
   const _fns = IS.array(first) ? first : [first, ...fns];

   return function (input: unknown) {
      let result = getMorph(_fns[0], input);

      for (let i = 1; i < _fns.length; i++) {
         if (IS.promise(result)) { // #Async switch#
            return asyncSteps(_fns.slice(i), result);
         }

         result = getMorph(_fns[i], result);
      }

      return result;
   }
}


async function asyncSteps(fns: TFMorph<unknown, unknown>[], result: Promise<unknown>) {
   let _result = await result;

   for (const fn of fns) {
      _result = await getMorph(fn, _result);
   }

   return result;
}