export function getProtoDescriptors(value: object):  {
   if (!value) return {};
   const proto = Object.getPrototypeOf(value);
   if (proto['constructor'] === Object || proto === null) return {};
   const descs = Object.entries(Object.getOwnPropertyDescriptors(proto))
      .filter(([key]) => key !== `constructor`);
   const replacer = { value: SHUTDOWN_LOCK, configurable: true, enumerable: true };
   const descriptors: Record<string, PropertyDescriptor> = {};

   for (const [key, desc] of descs) {
      const replace = (`value` in desc) && key !== `watch`;
      const _desc: PropertyDescriptor = replace ? replacer : { ...desc, enumerable: true };
      descriptors[key] = _desc;
   }

   return { ...descriptors, ...this.getProtoDescriptors(proto) };
}
