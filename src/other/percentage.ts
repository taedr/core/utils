export function getPercentage(size: number, index: number) {
   const onePrc = 100 / size;
   const progress = onePrc * (index + 1);
   const rounded = progress - Math.floor(progress) > 0 ? +progress.toFixed(3) : progress;

   return rounded;
}