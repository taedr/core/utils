import { TMapper } from '../general/types';
import { IS } from './typeof';

export type TSMorph<I, O> = TMapper<I, O> | O;
export type TAMorph<I, O> =  TMapper<I, Promise<O>> | Promise<O> | O;
export type TFMorph<I, O> = TMapper<I, Promise<O>> | TMapper<I, O>;
export type TMorph<I, O> = TMapper<I, Promise<O>> | TMapper<I, O> | Promise<O> | O;


export function getMorph<I, O>(polymorph: TMorph<I, O>, input: I): O | Promise<O> {
   return getSyncMorph(<TSMorph<I, O>>polymorph, input);
}


export function getSyncMorph<I, O>(polymorph: TSMorph<I, O>, input: I): O {
   return IS.function(polymorph) ? polymorph(input) : polymorph;
}