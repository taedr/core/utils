import { TNew } from '../general/types';
import { IId } from './id';

export enum EType {
   number,
   bigint,
   string,
   boolean,
   array,
   map,
   object,
   function,
   symbol,
   null,
   unknown
}

export type TETypeByType<T> =
   T extends string ? EType.string :
   T extends number ? EType.number :
   T extends boolean ? EType.boolean :
   T extends bigint ? EType.bigint :
   T extends null ? EType.null :
   T extends undefined ? EType.null :
   T extends symbol ? EType.symbol :
   T extends Function ? EType.function :
   T extends object ? T extends Record<string, T[keyof T]> ? EType.map :
   T extends unknown[] ? EType.array : EType.object : EType.unknown;

export type TTypeByEType<T extends EType> =
   T extends EType.string ? string :
   T extends EType.number ? number :
   T extends EType.boolean ? boolean :
   T extends EType.bigint ? bigint :
   T extends EType.symbol ? symbol :
   T extends EType.array ? unknown[] :
   T extends EType.map ? Record<string, unknown> :
   T extends EType.function ? Function :
   T extends EType.object ? object :
   T extends EType.null ? null : unknown;

export type TStringKeys<T extends object> = Extract<keyof T, string>;
export type TNumberKeys<T extends object> = Extract<keyof T, number>;
export type TSymbolKeys<T extends object> = Extract<keyof T, symbol>;

export function getType(value: unknown): EType {
   if (IS.array(value)) return EType.array;
   if (IS.null(value)) return EType.null;
   const type: any = typeof value;
   return EType[type] as any;
}


export const IS = Object.freeze({
   string(value: unknown): value is string {
      return typeof value === 'string';
   },
   number(value: unknown): value is number {
      return typeof value === 'number' && !isNaN(value);
   },
   boolean(value: unknown): value is boolean {
      return typeof value === 'boolean';
   },
   bigInt(value: unknown): value is bigint {
      return typeof value === 'bigint';
   },
   primitive(value: unknown): boolean {
      return IS.string(value) || IS.number(value) || IS.boolean(value) || IS.bigInt(value);
   },
   object(value: unknown): value is object {
      return typeof value === 'object' && !IS.null(value);
   },
   array(value: unknown): value is Array<unknown> {
      return Array.isArray(value);
   },
   readonlyArray(value: unknown): value is readonly unknown[] {
      return Array.isArray(value);
   },
   symbol(value: unknown): value is symbol {
      return typeof value === 'symbol';
   },
   function(value: unknown): value is Function {
      return typeof value === 'function';
   },
   null(value: unknown): value is null | undefined {
      return value === null || value === undefined;
   },
   promise(value: unknown): value is Promise<unknown> {
      return value instanceof Promise;
   },
   iterable(value: any): value is Iterable<unknown> {
      return typeof value === 'object' && value !== null && Symbol.iterator in value;
   },
   type: {
      primitive(type: EType): boolean {
         return type === EType.string || type === EType.number || type === EType.boolean || type === EType.bigint;
      },
      object(type: EType): boolean {
         return type === EType.object || IS.type.iterable(type);
      },
      iterable(type: EType): boolean {
         return type === EType.array || type === EType.map;
      },
      null(type: EType): boolean {
         return type === EType.null;
      },
   },
   running: {
      browser: typeof window !== 'undefined',
      node: typeof process !== 'undefined',
      worker: typeof window === 'undefined' && typeof process === 'undefined',
   },
   equal: {
      json<T>(a: T, b: T): boolean {
         if (a === b) return true;
         return JSON.stringify(a) === JSON.stringify(b);
      },
      direct<T>(a: T, b: T): boolean {
         return a === b;
      },
      id(a: IId, b: IId): boolean {
         return a.id + `` === b.id + ``;
      },
      type(a: unknown, b: unknown): boolean {
         return getType(a) === getType(b);
      },
   },
   custom: {
      builder(builder: Function): boolean {
         builder //?
         const native = new Set([Object, String, Number, Boolean, Function, Array, Symbol]);
         return !!builder && !native.has(builder as any);
      },
      class(value: unknown): boolean {
         return IS.object(value) && IS.custom.builder(value['constructor'] as TNew<object>);
      }
   }
});
