import { IS } from './typeof';

export type TPack<T> = T | [T, ...T[]];
export type TZPack<T> = T | T[];


export function getPack<T>(pack: TPack<T>): [T, ...T[]] {
   return getZPack(pack) as [T, ...T[]];
}


export function getZPack<T>(pack: TZPack<T>): T[] {
   const values = Array.isArray(pack) ? pack : [ pack ];
   const filtred = values.filter(val => !IS.null(val));

   return filtred;
}