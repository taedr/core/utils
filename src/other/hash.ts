import { IS } from './typeof';


export const HASH = Symbol(`HASH`);


export interface IHashed {
   readonly [HASH]: number;
}


export const FN_EQUAL_HASH = (a: unknown, b: unknown, isSave?: boolean): boolean => {
   return getHash(a, isSave) === getHash(b, isSave);
}


export function getHash(value: any, isSave = false): number {
   if (value[HASH]) return value[HASH];

   const toHash = IS.string(value) ? value : JSON.stringify(value);
   const len = toHash.length;
   let hash = 0;

   for (let i = 0; i < len; i++) {
      hash = Math.imul(31, hash) + toHash.charCodeAt(i) | 0;
   }

   if (isSave && IS.object(value)) (<any>value)[HASH] = hash;

   return hash;
}
