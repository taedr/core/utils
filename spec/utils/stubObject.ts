import { FN_RANDOM_NUM } from '@taedr/utils';

export interface IObjectConfig {
   id?: number;
   someFields?: boolean;
   nesting?: number;
}

export interface IStubObject {
   id: number;
   num: number;
   str: string;
   bool: boolean;
   arr: unknown[];
   obj: IStubObject | null;
}


export function getStubObject(config: IObjectConfig = {}) {
   const { id, someFields, nesting } = config;
   const obj: IStubObject = {
      id: id || 0,
      num: 1,
      str: `1`,
      bool: true,
      arr: [1, '1', true],
      obj: null
   };

   const nest = nesting || 0
   const fileds = Object.keys(obj);

   if (nest) {
      obj.obj = getStubObject({
         ...config,
         nesting: nest - 1,
      });
   }

   if (someFields) {
      const toDelCount = FN_RANDOM_NUM(0, fileds.length - 1);
      for (let i = 0; i < toDelCount; i++) {
         const delIndex = FN_RANDOM_NUM(0, fileds.length - 1);
         const key = fileds[delIndex];
         delete obj[key];
      }
   }

   return obj;
}
