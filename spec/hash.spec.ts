import { getHash, FN_RANDOM_NUM, FN_PACK } from "@taedr/utils";
import { getStubObject } from './utils/stubObject';

describe(`Hash`, () => {
   it(`Equal`, async () => {
      const obj_1 = getStubObject({ nesting: 2 });
      const obj_2 = getStubObject({ nesting: 2 });
      const hash_1 = getHash(JSON.stringify(obj_1));
      const hash_2 = getHash(obj_2);

      expect(hash_1 === hash_2).toEqual(true);
   });


   it(`Not Equal`, async () => {
      const obj_1 = getStubObject({ nesting: 2 });
      const obj_2 = getStubObject({ nesting: 3 });
      const hash_1 = getHash(obj_1);
      const hash_2 = getHash(JSON.stringify(obj_2));

      expect(hash_1 === hash_2).toEqual(false);
   });


   it(`Performance`, async () => {
      const count = 1000;
      const array = FN_PACK(count).map(() => getStubObject({ nesting: 2 }));

      const start = performance.now();
      for (let i = 0; i < array.length; i++) {
         getHash(array[i]);
      }
      const duration = performance.now() - start;

      duration //?
      array    //?


      const start_1 = performance.now();
      for (let i = 0; i < array.length; i++) {
         getHash(array[i]);
      }
      const duration_2 = performance.now() - start_1;


      duration_2 //?

   });
});