import { notFoundDec, SeacrhError } from "@taedr/utils";

describe(`Decorators`, () => {
   class Searcher<T> {
      constructor(
         protected _values: T[] = []
      ) { }


      @notFoundDec
      public getHead() { return this._values[0]; }
      @notFoundDec
      public getTail() { return this._values[this._values.length - 1]; }
      @notFoundDec
      public getIndex(index: number) { return this._values[index]; }
   }

   it(`Valid`, async () => {
      const searcher = new Searcher([1, 2, 3, 4, 5]);

      searcher //?

      expect(searcher.getHead()).toEqual(1);
      expect(searcher.getTail()).toEqual(5);
      expect(searcher.getIndex(2)).toEqual(3);
   });


   it(`Error`, async () => {
      const searcher = new Searcher([]);
      const error = new SeacrhError();

      expect(() => searcher.getHead()).toThrowError(error);
      expect(() => searcher.getTail()).toThrowError(error);
      expect(() => searcher.getIndex(2)).toThrowError(error);
   });
});