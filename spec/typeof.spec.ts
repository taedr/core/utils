import { EType, IS, getType } from '@taedr/utils';

describe(`typeof`, () => {
   class Test {
      constructor(
         readonly id: string
      ) {}
   }

   const number = 0;
   const string = ``;
   const boolean = false;
   const bigInt = BigInt(0);
   const symbol = Symbol(`test`);
   const array = [];
   const object = {};
   const custom = new Test(`0`);
   const builder = Test;
   const _function = function() {};
   const _null = null;
   const _undefined = undefined;
   const promise = new Promise(() => {});
   const test_1 = custom;
   const test_2 = new Test(`1`);


   it(`Number`, () => {
      expect(IS.number(number)).toBeTruthy();
      expect(IS.number(string)).toBeFalsy();
      expect(IS.number(NaN)).toBeFalsy();
      expect(!IS.number(boolean)).toBeTruthy();
   });

   it(`String`, () => {     
      expect(IS.string(string)).toBeTruthy();
      expect(IS.string(number)).toBeFalsy();
      expect(!IS.string(boolean)).toBeTruthy();
   });

   it(`Boolean`, () => {     
      expect(IS.boolean(boolean)).toBeTruthy();
      expect(IS.boolean(number)).toBeFalsy();
      expect(!IS.boolean(string)).toBeTruthy();
   });

   it(`bigInt`, () => {     
      expect(IS.bigInt(bigInt)).toBeTruthy();
      expect(IS.bigInt(number)).toBeFalsy();
      expect(!IS.bigInt(string)).toBeTruthy();
   });

   it(`primitive`, () => {     
      expect(IS.primitive(number)).toBeTruthy();
      expect(IS.primitive(string)).toBeTruthy();
      expect(IS.primitive(boolean)).toBeTruthy();
      expect(IS.primitive(bigInt)).toBeTruthy();
      expect(IS.primitive(object)).toBeFalsy();
      expect(!IS.primitive(_null)).toBeTruthy();
   });

   it(`symbol`, () => {     
      expect(IS.symbol(symbol)).toBeTruthy();
      expect(IS.symbol(number)).toBeFalsy();
      expect(!IS.symbol(string)).toBeTruthy();
   });

   it(`function`, () => {     
      expect(IS.function(_function)).toBeTruthy();
      expect(IS.function(number)).toBeFalsy();
      expect(!IS.function(string)).toBeTruthy();
   });

   it(`null`, () => {     
      expect(IS.null(_null)).toBeTruthy();
      expect(IS.null(number)).toBeFalsy();
      expect(!IS.null(string)).toBeTruthy();
   });

   it(`null`, () => {     
      expect(IS.null(_undefined)).toBeTruthy();
      expect(IS.null(_null)).toBeTruthy();
      expect(IS.null(number)).toBeFalsy();
      expect(!IS.null(string)).toBeTruthy();
   });

   it(`object`, () => {     
      expect(IS.object(object)).toBeTruthy();
      expect(IS.object(_null)).toBeFalsy();
      expect(!IS.object(string)).toBeTruthy();
   });

   it(`array`, () => {     
      expect(IS.array(array)).toBeTruthy();
      expect(IS.array(object)).toBeFalsy();
      expect(!IS.array(string)).toBeTruthy();
   });

   it(`iterable`, () => {     
      expect(IS.iterable(array)).toBeTruthy();
      expect(IS.iterable(string)).toBeTruthy();
      expect(IS.iterable(object)).toBeFalsy();
      expect(!IS.iterable(boolean)).toBeTruthy();
   });

   it(`custom class`, () => {       
      expect(IS.custom.class(custom)).toBeTruthy();
      expect(IS.custom.class(object)).toBeFalsy();
      expect(!IS.custom.class(array) ).toBeTruthy();
   });

   it(`custom builder`, () => {     
      expect(IS.custom.builder(builder)).toBeTruthy();
      expect(IS.custom.builder(object['constructor'])).toBeFalsy();
      expect(!IS.custom.builder(array['constructor'])).toBeTruthy();
   });

   it(`same type`, () => {     
      expect(IS.equal.type(number, number)).toBeTruthy();
      expect(IS.equal.type(custom, object)).toBeTruthy();
      expect(IS.equal.type(number, bigInt)).toBeFalsy();
      expect(!IS.equal.type(array, object)).toBeTruthy();
   });

   it(`promise`, () => {     
      expect(IS.promise(promise)).toBeTruthy();
      expect(IS.promise(bigInt)).toBeFalsy();
      expect(!IS.promise(object)).toBeTruthy();
   });

   it(`equal`, () => {     
      expect(IS.equal.json(test_1, custom)).toBeTruthy();
      expect(IS.equal.json(number, number)).toBeTruthy();
      expect(!IS.equal.json(test_1, test_2)).toBeTruthy();

      expect(IS.equal.id(test_1, custom)).toBeTruthy();
      expect(!IS.equal.id(test_1, test_2)).toBeTruthy();

      expect(IS.equal.direct(test_1, custom)).toBeTruthy();
      expect(!IS.equal.direct(test_1, test_2)).toBeTruthy();
   });

   it(`type`, () => {     
      expect(getType(number)).toBe(EType.number);
      expect(getType(string)).toBe(EType.string);
      expect(getType(boolean)).toBe(EType.boolean);
      expect(getType(bigInt)).toBe(EType.bigint);
      expect(getType(symbol)).toBe(EType.symbol);
      expect(getType(array)).toBe(EType.array);
      expect(getType(object)).toBe(EType.object);
      expect(getType(custom)).toBe(EType.object);
      expect(getType(_function)).toBe(EType.function);
      expect(getType(builder)).toBe(EType.function);
      expect(getType(_null)).toBe(EType.null);
      expect(getType(_undefined)).toBe(EType.null);
   });

   it(`primitive type`, () => {     
      expect(IS.type.primitive(EType.string)).toBeTruthy();
      expect(IS.type.primitive(EType.number)).toBeTruthy();
      expect(IS.type.primitive(EType.boolean)).toBeTruthy();
      expect(IS.type.primitive(EType.bigint)).toBeTruthy();
   });

   it(`object type`, () => {     
      expect(IS.type.object(EType.object)).toBeTruthy();
      expect(IS.type.object(EType.array)).toBeTruthy();
      expect(IS.type.object(EType.map)).toBeTruthy();
      expect(!IS.type.object(EType.null)).toBeTruthy();
   });

   it(`iterable type`, () => {     
      expect(IS.type.iterable(EType.array)).toBeTruthy();
      expect(IS.type.iterable(EType.map)).toBeTruthy();
      expect(!IS.type.iterable(EType.object)).toBeTruthy();
   });
})