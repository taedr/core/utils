import { getStaticChain } from "@taedr/utils";

describe(`Static Chain`, () => {
   const marker = Symbol(`Marker`);

   it(`Straight`, () => {
      class Parrent {
         static [marker] = 1;
      }

      class Child extends Parrent {
         static [marker] = 2;
      }

      class GrandChild  extends Child {
         static [marker] = 3;
      }

      const expected = [Parrent[marker], Child[marker], GrandChild[marker]];
      const chain = getStaticChain(GrandChild, marker);

      console.log(chain)

      expect(chain).toEqual(expected);
   });


   it(`Gap`, () => {
      class Parrent {
         static [marker] = 1;
      }

      class Child extends Parrent {
      }

      class GrandChild  extends Child {
         static [marker] = 3;
      }

      const expected = [Parrent[marker], GrandChild[marker]];
      const chain = getStaticChain(GrandChild, marker);

      console.log(chain)

      expect(chain).toEqual(expected);
   });
});