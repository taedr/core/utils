import { FN_PACK, FN_RANDOM_NUM, IMap, performanceMs } from '@taedr/utils';

describe(`Map`, () => {
   const count = 10000;
   const indexes = FN_RANDOM_NUM({ min: 0, max: count, size: count / 2 });
   const strings = FN_PACK(count).map(() => {
      const size = FN_RANDOM_NUM(3, 10);
      const codes = FN_RANDOM_NUM({ min: 0, max: 100, size });
      const str = String.fromCharCode(...codes);

      return str;
   });


   it(``, async () => {
      const plain: IMap<string> = {};
      const map = new Map<string, string>();


      const map_set = performanceMs(() => {
         for (let i = 0; i < strings.length; i++) {
            const str = strings[i];
            map.set(str, str);
         }
      })

      const plain_set = performanceMs(() => {
         for (let i = 0; i < strings.length; i++) {
            const str = strings[i];
            plain[str] = str;
         }
      })


      const map_get = performanceMs(() => {
         for (let i = 0; i < indexes.length; i++) {
            const index = indexes[i];
            const str = strings[index];
            const value = map.get(str);
         }
      })


      const plain_get = performanceMs(() => {
         for (let i = 0; i < indexes.length; i++) {
            const index = indexes[i];
            const str = strings[index];
            const value = plain[str];
         }
      })


      plain_set.duration   //?
      map_set.duration     //?

      plain_get.duration   //?
      map_get.duration     //?
   });
});