import { getId } from "@taedr/utils"

it(`Id`, () => {
   const count = 100000;
   const ids = new Set();

   for (let i = 0; i < count; i++) {
      ids.add(getId());
   }

   expect(ids.size).toEqual(count);
});
