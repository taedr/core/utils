import { TMorph, getMorph, TSMorph, TAMorph, getSyncMorph, FN_RANDOM_NUM } from "@taedr/utils";

describe(`Morph`, () => {
   const val: TSMorph<void, number> = FN_RANDOM_NUM(10, 100);
   const modif = FN_RANDOM_NUM(10, 100);
   const fnResult = val * modif;

   const promise: TAMorph<void, number> = Promise.resolve(val);
   const mapper: TSMorph<number, number> = (modif: number) => val * modif;
   const asyncMapper: TMorph<number, number> = async (modif: number) => mapper(modif);

   it(`Sync`, async () => {
      expect(
         getSyncMorph(val, null)
      ).toEqual(val);
      expect(
         getSyncMorph(mapper, modif)
      ).toEqual(fnResult);
   });

   it(`Async`, async () => {
      expect(
         await getMorph(val, null)
      ).toEqual(val);
      expect(
         await getMorph(val, null)
      ).toEqual(val);
      expect(
         await getMorph(promise, null)
      ).toEqual(val);
      expect(
         await getMorph(mapper, modif)
      ).toEqual(fnResult);
      expect(
         await getMorph(asyncMapper, modif)
      ).toEqual(fnResult);
   });
});