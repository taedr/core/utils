import { getValueByPath } from "@taedr/utils";
import { getStubObject } from './utils/stubObject';

describe(`Value by path`, () => {
   const obj = getStubObject({ nesting: 2 });

   it(`Val`, () => {
      const path = [`obj`, `arr`, `1`];
      const expected = obj.obj.arr[1];
      const value = getValueByPath(obj, path);

      expect(value).toEqual(expected);
   });


   it(`Val`, () => {
      const path = [`obj`, `obj`, `obj`, `num`];
      const expected = null;
      const value = getValueByPath(obj, path);

      expect(value).toEqual(expected);
   });


   it(`Empty path`, () => {
      const path = [];
      const expected = obj;
      const value = getValueByPath(obj, path);

      expect(value).toEqual(expected);
   });


   it(`Path Separator`, () => {
      const path = [`obj`, `arr`, `1`].join(`.`);
      const expected = obj.obj.arr[1];
      const value = getValueByPath(obj, path);

      expect(value).toEqual(expected);
   });
});