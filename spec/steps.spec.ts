import { steps, FN_PACK } from '@taedr/utils';

describe(steps.name, () => {
   class Car {
      public wheels: Wheel[] = null;
      public doors: Door[] = null;
      public color: string = null;
   }

   class Wheel { }
   class Door { }


   it(`Sync`, async () => {
      class Value {
         readonly value: string;
      }

      function multiply(value: number): number {
         return value * 2;
      }

      function booler(value: number): boolean {
         return value % 3 === 0;
      }

      function colorise(value: boolean): string {
         return value ? 'green' : 'red';
      }

      const value = steps(
         multiply,
         booler,
         colorise
      )(1);

      expect(value).toBe(`red`);
   })



   it(`Async`, async () => {
      function wheels(car: Car): Car {
         car.wheels = FN_PACK(4).map(() => new Wheel());
         return car;
      }

      async function doors(car: Car): Promise<Car> {
         car.doors = FN_PACK(2).map(() => new Door());
         return car;
      }

      function color(car: Car): Car {
         car.color = `green`;
         return car;
      }

      const car = await steps(
         wheels,
         doors,
         color
      )(new Car());

      expect(car.wheels).toEqual(FN_PACK(4).map(() => new Wheel()));
      expect(car.doors).toEqual(FN_PACK(2).map(() => new Door()));
      expect(car.color).toBe(`green`);
   });
})
