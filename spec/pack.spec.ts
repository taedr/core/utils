import { TPack, getPack } from "@taedr/utils";

describe(`Pack`, () => {
   it(`Val`, () => {
      const morph: TPack<number> = 1;
      const val = getPack(morph);

      expect(val).toEqual([1]);
   });


   it(`Array`, () => {
      const morph: TPack<number> = [1, 2, 3, 5];
      const val = getPack(morph);

      expect(val).toEqual(morph);
   });


   it(`Nulls`, () => {
      const morph: TPack<number> = [1, null, 3, null, 5];
      const val = getPack(morph);

      expect(val).toEqual([1, 3, 5]);
   });
});