import { Switch, Hub, FN_NULL, SWITCH_ERRORS } from "@taedr/utils";

describe(Switch.name, () => {
   it(`ON / OFF`, () => {
      const on: number[] = [];
      const off: number[] = [];

      const hub = new Hub();
      const switch_1 = new Switch({
         on: () => on.push(1),
         off: () => off.push(1),
      });
      const switch_2 = new Switch({
         on: () => on.push(2),
         off: () => off.push(2),
      });

      hub.attach(switch_1);
      hub.attach(switch_2);

      expect(hub['_switchers'].size).toBe(2);

      hub.state = 'ON';
      hub.state = 'ON';
      hub.state = 'OFF';
      hub.state = 'OFF';

      expect(switch_1.isOff).toBeTruthy();
      expect(switch_2.isOff).toBeTruthy();

      hub.state = 'ON';

      expect(switch_2.isOn).toBeTruthy();
      hub.detach(switch_2);
      expect(hub['_switchers'].size).toBe(1);
      expect(switch_2.isOff).toBeTruthy();

      hub.state = 'ON';
      hub.state = 'OFF';
      hub.state = 'ON';

      expect(switch_1.isOn).toBeTruthy();
      hub.detach(switch_1);
      expect(hub['_switchers'].size).toBe(0);
      expect(switch_1.isOff).toBeTruthy();


      hub.state = 'ON';
      hub.state = 'OFF';
      hub.state = 'ON';

      console.log(hub.state)
      console.log(switch_1)

      hub.attach(switch_1);
      hub.attach(switch_2);

      expect(hub['_switchers'].size).toBe(2);
      expect(switch_1.isOn).toBeTruthy();
      expect(switch_2.isOn).toBeTruthy();

      console.log(hub.state)

      hub.detach(switch_1);
      hub.detach(switch_2);

      hub.state = 'ON';
      hub.state = 'OFF';

      console.log(hub)
      console.log(on)
      console.log(off)

      expect(on).toEqual([1, 2, 1, 2, 1, 1, 2]);
      expect(off).toEqual([1, 2, 2, 1, 1, 1, 2]);
      expect(hub.isOff).toBeTruthy();
      expect(switch_1.state).toBe(`OFF`);
      expect(switch_1.state).toBe(`OFF`);
      expect(hub['_switchers'].size).toBe(0);
   });

   it(`Switcher Attach`, () => {
      const hub_1 = new Hub();
      const hub_2 = new Hub();
      const switch_1 = new Switch({
         on: FN_NULL,
         off: FN_NULL,
      });

      hub_1.attach(switch_1);

      const switch_2 = hub_1.attach({
         on: FN_NULL,
         off: FN_NULL,
      });

      console.log(hub_1);
      console.log(hub_2);

      expect(hub_1['_switchers'].size).toBe(2);

      hub_1.detach(switch_2);
      hub_2.attach(switch_2);

      hub_1.detach(switch_1);
      switch_1.state = 'ON';
      hub_1.attach(switch_1);

      expect(
         () => switch_1.state = 'ON'
      ).toThrowError(
         SWITCH_ERRORS.manual
      );

      expect(
         () => hub_1.attach(switch_1)
      ).toThrowError(
         SWITCH_ERRORS.few
      );

      expect(
         () => hub_2.attach(switch_1)
      ).toThrowError(
         SWITCH_ERRORS.few
      );

      expect(
         () => hub_1.attach(switch_2)
      ).toThrowError(
         SWITCH_ERRORS.few
      );
   });


   it(`Hub Attach`, () => {
      const hub_0 = new Hub();
      const hub_1 = new Hub();
      const hub_2 = new Hub();

      hub_1.attach(hub_0);
      hub_0.attach(hub_2);

      expect(
         () => hub_0.attach(hub_0)
      ).toThrowError(
         SWITCH_ERRORS.itself
      );

      expect(
         () => hub_2.attach(hub_1)
      ).toThrowError(
         SWITCH_ERRORS.recursive
      );

      console.log(hub_0);
      console.log(hub_1);
      console.log(hub_2);
   });


   it(`Wrappers`, () => {
      
      
      const hub = new Hub();

      hub //?
 
      
   });
});