> Note that API is on the prototype stage and can be changed dramatically without backward compatibility support!

## Overview
Utils package contains additional commonly used functionality across [@taedr](https://www.npmjs.com/search?q=%40taedr) packages.